package com.mdstatparser.alex.mdstatparser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    private String mdStat = "Personalities : [raid1] [raid6] [raid5] [raid4]\n" +
            "md1 : active raid1 sdb2[1] sda2[0]\n" +
            "136448 blocks [2/2] [UU]\n" +
            "\n" +
            "md2 : active raid1 sdb3[1] sda3[0]\n" +
            "129596288 blocks [2/2] [UU]\n" +
            "\n" +
            "md3 : active raid5 sdl1[9] sdk1[8] sdj1[7] sdi1[6] sdh1[5] sdg1[4] sdf1[3] sde1[2] sdd1[1] sdc1[0]\n" +
            "1318680576 blocks level 5, 1024k chunk, algorithm 2 [10/10] [UUUUUUUUUU]\n" +
            "\n" +
            "md0 : active raid1 sdb1[1] sda1[0]\n" +
            "16787776 blocks [2/2] [UU]\n" +
            "\n" +
            "unused devices: <none> ";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        HashMap<String, Object> parsed = MdStatParser.mdStatParse(mdStat);
        System.out.print(parsed);
    }
}
