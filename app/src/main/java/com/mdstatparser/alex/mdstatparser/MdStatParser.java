package com.mdstatparser.alex.mdstatparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class MdStatParser {

    public static HashMap<String, Object> mdStatParse(String mdStat){
        HashMap<String, Object> result = new HashMap<>();
        StringBuffer mdStatBuffer = new StringBuffer(mdStat);


        String personalities = parsePersonalities(mdStatBuffer, mdStat);
        mdStat = String.valueOf(mdStatBuffer);

        ArrayList<String> raids = parseRaids(mdStatBuffer, mdStat);
        mdStat = String.valueOf(mdStatBuffer);

        result.put(personalities, raids);

        ArrayList<String> body = parseBody(mdStat);

        parseBodyToMap(body, result);

        System.out.println(mdStatBuffer);

        return result;
    }

    private static String parsePersonalities (StringBuffer mdStatBuffer, String mdStat) {
        String[] parts = mdStat.split(":");
        mdStatBuffer.delete(0, parts[0].length() + 2);
        StringBuffer buffer = new StringBuffer(parts[0]);
        buffer.delete(buffer.length() - 1, buffer.length());
        return String.valueOf(buffer);
    }

    private static ArrayList<String> parseRaids (StringBuffer mdStatBuffer, String mdStat){
        String[] parts = mdStat.split("\n");
        mdStatBuffer.delete(0, parts[0].length() + 1);

        String [] raids = parts[0].split(" ");

        for (int i = 0; i < raids.length; i++){
            StringBuffer buffer = new StringBuffer(raids[i]);
            buffer.delete(0,1);
            buffer.delete(raids[i].length() - 2, raids[i].length());
            raids[i] = String.valueOf(buffer);
        }
        ArrayList<String> raidsList = new ArrayList<>(Arrays.asList(raids));
        return raidsList;
    }

    private static ArrayList<String> parseBody (String mdStat){
        String[] parts = mdStat.split("\n");
        ArrayList<String> partsList = new ArrayList<>(Arrays.asList(parts));
        ArrayList<String> clone = new ArrayList<>(partsList);
        for (String item: partsList){
            if (item.equals(""))
                clone.remove(item);
        }
        return clone;
    }



    private static void parseBodyToMap(ArrayList<String> body, HashMap<String, Object> result){
        int i = 0;
        ArrayList<String> blocks = new ArrayList<>();
        for (String item :
                body) {
            String[] mds = item.split(":");
            ArrayList<String> mdsList = new ArrayList<>(Arrays.asList(mds));

            if (mdsList.size() > 1) {
                result.put(mdsList.get(0) + i, parseParsedBody(mdsList.get(1)));
            } else
                result.put("status " + i++, parseParsedBody(mdsList.get(0)));

           System.out.print(blocks);
        }
    }

    private static ArrayList<String> parseParsedBody(String stringToParse){
        String[] data = stringToParse.split(" ");
        ArrayList<String> dataList = new ArrayList<>(Arrays.asList(data));
        ArrayList<String> clone = new ArrayList<>(dataList);
        for (String dt: dataList){
            if (dt.equals(""))
                clone.remove(dt);
        }

        return clone;

    }


}
